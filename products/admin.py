from django.contrib import admin
from products.models import Product, Brand


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active', 'brand', 'color')


class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'active_products')

    def active_products(self, obj):
        return obj.product_set.filter(is_active=True).count()
    active_products.short_description = 'Кол-во активных товаров'


admin.site.register(Product, ProductAdmin)
admin.site.register(Brand, BrandAdmin)
