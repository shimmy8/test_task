from django.db import models


class Brand(models.Model):
    name = models.CharField(max_length=100, verbose_name='Наименование')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Производтель'
        verbose_name_plural = 'Производтели'


PRODUCT_COLOR_CHOISES = (
    ('red', 'Красный'),
    ('blue', 'Синий'),
    ('green', 'Зеленый')
)


class Product(models.Model):
    name = models.CharField(max_length=250, verbose_name='Наименование')
    brand = models.ForeignKey(Brand, null=True, blank=True, verbose_name='Производитель', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True, verbose_name='Активен')
    color = models.CharField(max_length=15, choices=PRODUCT_COLOR_CHOISES, null=True, blank=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
